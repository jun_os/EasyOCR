package com.neuralnetwork.sample.ui;

import java.awt.Rectangle;

import javax.swing.JFrame;

public class RasterPanel extends JFrame{
	private int width = 1350;
    private int height = 300;
    private Canvas canvas = null;
    
    public RasterPanel(){
    	super();
        this.setTitle("EasyOCR Raster Panel");
        this.setSize(width, height);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocation(350, 650);
        this.setLayout(null);
        
        this.canvas = new Canvas(1100,100);
        this.canvas.setBounds(new Rectangle(75, 30, 1100,100));
        this.add(this.canvas);
        
        this.setVisible(true);
    }
}
